# Copyright (C) 2009 The Android Open Source Project
# Copyright (c) 2011, The Linux Foundation. All rights reserved.
# Copyright (C) 2017-2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Missing parts: gz, logo

import re

def FullOTA_InstallBegin(info):
  info.script.AppendExtra('assert(update_dynamic_partitions(package_extract_file("install/firmware-update/dynamic_partitions_main_list")));')
  return

def FullOTA_InstallEnd(info):
  OTA_UpdateFirmware(info)
  return

def OTA_UpdateFirmware(info):
  info.script.AppendExtra('ui_print("Flashing vendor and firmware images");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/vendor.img", map_partition("vendor"));')
  info.script.AppendExtra('package_extract_file("install/firmware-update/cdt_engineering.img", "/dev/block/platform/bootdevice/by-name/cdt_engineering");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/dtbo.img", "/dev/block/platform/bootdevice/by-name/dtbo");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/lk.img", "/dev/block/platform/bootdevice/by-name/lk");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/lk.img", "/dev/block/platform/bootdevice/by-name/lk2");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/md1img.img", "/dev/block/platform/bootdevice/by-name/md1img");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/preloader_emmc.img", "/dev/block/sda");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/preloader_emmc.img", "/dev/block/sdb");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/scp.img", "/dev/block/platform/bootdevice/by-name/scp1");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/scp.img", "/dev/block/platform/bootdevice/by-name/scp2");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/spmfw.img", "/dev/block/platform/bootdevice/by-name/spmfw");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/sspm.img", "/dev/block/platform/bootdevice/by-name/sspm_1");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/sspm.img", "/dev/block/platform/bootdevice/by-name/sspm_2");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/tee.img", "/dev/block/platform/bootdevice/by-name/tee1");')
  info.script.AppendExtra('package_extract_file("install/firmware-update/tee.img", "/dev/block/platform/bootdevice/by-name/tee2");')
